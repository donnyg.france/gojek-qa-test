package cucumberTest;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import page.GojekPage;

import java.util.concurrent.TimeUnit;

public class BaseAcceptanceTest {

    protected static WebDriver driver;

    @Before
    public void setup() {
       System.setProperty("webdriver.chrome.driver", "src/test/resources/mac/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Given("^I open website gojek$")
    public void i_open_website_gojek()throws Throwable {
       driver.get(GojekPage.webUrl);
        // Write code here that turns the phrase above into concrete actions
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        throw new PendingException();
    }

    @Given("^I click layanan button$")
    public void i_click_layanan_button() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.xpath("//A[@id='productButton']")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        throw new PendingException();
    }

    @When("^I click images GO-RIDE$")
    public void i_click_images_GO_RIDE() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.xpath("(//A[@href='/go-ride/'])[2]")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        throw new PendingException();
    }

    @Then("^I should see 'Kenapa GO-RIDE'$")
    public void i_should_see_Kenapa_GO_RIDE() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.xpath("//H4[@class='section--title']"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        throw new PendingException();
    }

    @When("^I click images GO-CAR$")
    public void i_click_images_GO_CAR() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.xpath("(//A[@href='/go-car/'])[2]")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        throw new PendingException();
    }

    @Then("^I should see 'Kenapa GO-CAR'$")
    public void i_should_see_Kenapa_GO_CAR() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.xpath("//H4[@class='section--title'][text()='KENAPA GO-CAR?']"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        throw new PendingException();
    }
    @When("^I click images GO-TIX$")
    public void i_click_images_GO_TIX() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.xpath(" (//A[@href='/go-tix/'])[2]")).click();
        throw new PendingException();
    }

    @Then("^I should see 'Cara Beli Tiket menggunakan GO-TIX'$")
    public void i_should_see_Cara_Beli_Tiket_menggunakan_GO_TIX() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.xpath("(//H4[text()='CARA BELI TIKET'][text()='CARA BELI TIKET'])[2]"));
        throw new PendingException();
    }

    @When("^I click images GO-FOOD$")
    public void i_click_images_GO_FOOD() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.xpath("(//A[@href='/go-food/'])[2]"));
        throw new PendingException();
    }

    @Then("^I should see 'Cara Menggunakan GO-FOOD'$")
    public void i_should_see_Cara_Menggunakan_GO_FOOD() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.xpath("//H3[@class='title'][text()='\n" +
                "          CARA MENGGUNAKAN GO-FOOD\n" +
                "      ']"));
        throw new PendingException();
    }

    @When("^I click images GO-SEND$")
    public void i_click_images_GO_SEND() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.xpath("(//A[@href='/go-send/'])[2]"));
        throw new PendingException();
    }

    @Then("^I should see 'Cara Menggunakan GO-SEND'$")
    public void i_should_see_Cara_Menggunakan_GO_SEND() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        driver.findElement(By.xpath("//H3[@class='title'][text()='\n" +
                "            CARA MENGGUNAKAN GO-SEND \n" +
                "        ']"));
        throw new PendingException();
    }
    @After
    public void teardown() {
        driver.quit();
    }
}
